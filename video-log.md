### [Three Views of Curved Awe Gripper (2023-03-15)](https://youtu.be/ZZK9m7KnYzI)

<img width=250 src=images/video-three-views-of-curved-awe-gripper.png />

`ai-data robot folder clothes gripper`

> *Trying to sprint to a simple gripper for use in v0 clothes folder.  Using a curved awe here and it seems to be working kind of.  If I add rotation in two dimensions I think I'll have something that I can get out of the gates with.  I know its not good for cloths to be pocked with a metal awe but there will be time for refinement later.  Right now I'm just trying to sprint to the point where I can start capturing data.  I think these three views will be enough.  Have to decide on resolutions and placement to keep from occluding important data.*

### [Curved Awe as Simple Gripper (2023-03-11)](https://youtu.be/rh7l3JPOSCA)

<img width=250 src=images/video-curved-awe-as-simple-gripper.png />

`ai-data robot folder clothes gripper`

> *Trying to sprint to a simple gripper for use in v0 clothes folder. Using a curved awe here and it seems to be working kind of. If I add rotation in two dimensions I think I'll have something that I can get out of the gates with. I know its not good for cloths to be poked with a metal awe but there will be time for refinement later. Right now I'm just trying to sprint to the point where I can start capturing data.*

### [Modified Keyboard Duster as Simple Gripper (2023-03-11)](https://youtu.be/zP-vopTi5KU)

<img width=250 src=images/video-modified-keyboard-duster-as-simple-gripper.png />

`ai-data robot folder clothes gripper`

> *Trying to sprint to a simple gripper for use in v0 clothes folder. I thought the suction produced by a cheaper keyboard duster might work but it does not. But, it seems like I can make some progress just just pushing the clothes around with a pointer.*
