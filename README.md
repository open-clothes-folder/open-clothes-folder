# Open Clothes Folder

Mechanicaly-lite compute-heavy unattended clothes folding approach.

## Concept
Folding clothes is significant and tedius work.  Machines exist but they seem to be limitted, complex and expensive (*Existing Attempts* below).  Many of the machines seem to mechanical-heavy, compute-lite attended devices.  This project is intended to result in a mechanicaly-lite, compute-heavy unattended device.  Imagine a $30 kids robotic arm with a $300 GPU and a couple of $20 cameras.  The idea is that the device does not have to be fast if it can be productive unattended.

<img width=300 src="images/kids-robotic-arm.png" /><img width=300 src="images/graphics-card.png" /><img width=300 src="images/camera.png" />

## Can a very simple device be productive?
To start to prove the concept I asked, "can I make progress folding a shirt with only my thumb and finger as a pincher on my right hand?"  I dropped a shirt from the dryer on my floor and tried to spread it out using only my right "pincher".  I was getting nowhere until I added a weight to the process (shown below in the red plastic case).  Using the weight, I could tug on various parts of the shirt to make adjustments.  I was able to get the shirt inside-in and spread out onto the carpet.

<img width=300 src="images/first-try.png" />

## How might this proceed?

The following illustration shows a two phased approach for how this project could proceed.  The first phase is intended to result in a stockpile of training data.  A set of manual, web-based folding rigs would be built and a plan would be put in place to entise people to log in and use the web-based controls to fold various items of clothing.  The video feed used to inform the users would be saved along with the inputs the users provide the folding rig.  These two components together comprise data that would be used to train an AI model in the second phase.  The final result would be a folding rig much like the initial manually controlled folding rigs but with no manual inputs required.

![](images/plan.png)

### Crowd Sourced Training Set Phase

Much of the initial work will be on the manual web based folding rig farm.

## AI Training Phase

The AI Training Phase will require AI expertise.

### Training Data

#### [You Tube Channel @openclothesfolder](https://www.youtube.com/channel/UCqtNjDHc3bj_llAFcraNcmw)

Started the above channel in the spirit of sprinting to data.  I don't expect that videos in this channel will be useful for AI training but making the first video has been useful in clearing thinking on the format of the real data.

<img width=400 src="images/youtube-channel.png" alt="youtube-channel" />

[Video Log](video-log.md)

## [Machine Learning](machine-learning.md)

## Contributing

Contributors welcome!  To contribute in any way or just to comment, please [add an issue to this project](https://gitlab.com/open-clothes-folder/open-clothes-folder/-/issues/new?issue%5Bmilestone_id%5D=).

## Project status
This is a background project for me as of 2022-03-23.  I hope to build momentum in the community and work on it more actively in the future.

2022-09-29 - there are two efforts running in parallel here.  One is very high level direction on what this project is and how it might get going.  The other is low level work on the manual web based folding rig.

## Existing Attempts

- [Attended Foldimate](https://foldimate.com)
- [$16,000 Landroid](https://www.youtube.com/watch?v=rl7iNRdTncQ)
- [Legit Industrial Robot](https://www.youtube.com/watch?v=8TsLkpPsdKo)
- [WV Sized Machine](https://www.youtube.com/watch?v=txBCFSaq_Cc)
- [Berkley Robot from 2010](https://www.youtube.com/watch?v=gy5g33S0Gzo)
- [Motorized Fold Plates](https://www.youtube.com/watch?v=rhWaHSUVGco)

## License

<img width=60 src="https://www.oshwa.org/wp-content/uploads/2014/03/oshw-logo-outline.svg"/> <a href="http://creativecommons.org/licenses/by-sa/4.0/deed.en_US"><img src="https://i0.wp.com/i.creativecommons.org/l/by-sa/4.0/88x31.png?w=790&ssl=1"/></a>

